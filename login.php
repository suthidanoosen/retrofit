<?php
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_POST['username']) && isset($_POST['password'])) {
 
    $username = $_POST['username'];
    $password = $_POST['password'];

	$host="localhost";      //replace with your hostname 
	$user="root";       //replace with your username 
	$passwd="";           //replace with your password 
	$database="retrofit";     //replace with your database 

	 try  
	{  
		$connect = new PDO("mysql:host=$host; dbname=$database", $user, $passwd);  
		$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
		
		$query = "SELECT * FROM users WHERE username = :username LIMIT 1";  
		$statement = $connect->prepare($query);  
		$statement->execute(  
			 array(  
				  'username'     =>     $_POST["username"]
			 )  
		);  
		$count = $statement->rowCount();  
		if($count > 0)  
		{  
			$result = $statement->fetch();

			//check password
			if (password_verify($_POST["password"], $result['password'])) {
				// successfully inserted into database
				$response["success"] = "true";
				$response["message"] = "Password is valid!";	
				
			} else {
				// invalid password
				$response["success"] = "false";
				$response["message"] = "Invalid password.";
		 
			} 
		}  
		else  
		{  
			// invalid password
			$response["success"] = "false";
			$response["message"] = "Invalid username.";
	 
		} 
				
	}
	catch(PDOException $error)  
	{  
		// PDO error
		$response["success"] = "false";
		$response["message"] = $error->getMessage();

	}  
} else {
    // required field is missing
    $response["success"] = "false";
    $response["message"] = "Required field(s) is missing";
	
}
// echoing JSON response
echo json_encode(array($response));
?>