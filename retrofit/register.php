<?php
 
// array for JSON response
$response = array();
 
// check for required fields
if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email'])) {
 
    $username = $_POST['username'];
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $email = $_POST['email'];
 
	$host="localhost";      //replace with your hostname 
	$user="root";       //replace with your username 
	$passwd="";           //replace with your password 
	$db_name="retrofit";     //replace with your database 

    //open connection to mysql db
    $connection = mysqli_connect($host,$user,$passwd,$db_name) or die("Error " . mysqli_error($connection));
 
    // mysql inserting a new row
	$sql = "INSERT INTO users(username, password, email) VALUES('$username', '$password', '$email')";
	$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	
 
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = "true";
        $response["message"] = "User account register successfull.";
 
        // echoing JSON response
        echo json_encode(array($response));
    } else {
        // failed to insert row
        $response["success"] = "false";
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode(array($response));
    }
} else {
    // required field is missing
    $response["success"] = "false";
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode(array($response));
}
?>